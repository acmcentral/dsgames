package org.acmcentral.minecraft.dsgames.game.state;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.acmcentral.minecraft.dsgames.thread.LocalizedBroadcast;
import org.bukkit.Bukkit;

public class PregameState extends State {
  
  public PregameState(Microplugin microplugin) {
    super(microplugin);
  }

  @Override public boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register) {
    LocalizedBroadcast broadcast = new LocalizedBroadcast()
        .addChatMessageBroadcast(getMicroplugin().getGameManager().getPlugin().getGamePlayers().values(), "Teleporting to arena...");
    Bukkit.getScheduler().scheduleSyncDelayedTask(getMicroplugin().getGameManager().getPlugin(), broadcast);
    scores.clear();
    return true;
  }
  
  @Override public boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds) {
    return false;
  }
  
}
