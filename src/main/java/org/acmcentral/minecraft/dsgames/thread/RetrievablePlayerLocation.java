package org.acmcentral.minecraft.dsgames.thread;

import java.util.concurrent.atomic.AtomicReference;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class RetrievablePlayerLocation implements Runnable {
  
  private AtomicReference<Location> location = null;
  private Player player = null;
  
  public RetrievablePlayerLocation(Player player) {
    this.location = new AtomicReference<>();
    this.player = player;
  }
  
  @Override public void run() {
    location.set(player.getLocation());
  }
  
  public Location getLocation() {
    try {
      for(;;) {
        Location location = this.location.get();
        if(location != null) return location;
        Thread.sleep(500L);
      }
    } catch(InterruptedException e) { }
    
    return null;
  }
}
