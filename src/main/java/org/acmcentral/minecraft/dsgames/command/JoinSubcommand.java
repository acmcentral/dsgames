package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.map.SerializableRegion;
import org.acmcentral.minecraft.dsgames.thread.MassPlayerTeleport;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public class JoinSubcommand extends Subcommand {

  public JoinSubcommand(CommandManager commandManager) {
    super(commandManager, "join an event");
    Map<String, String> usages = new HashMap<>();
    usages.put("", "join an event");
    setUsages(usages);
  }

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(sender instanceof Player) {
      sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
          "&2[&4DSGames&2] &cThis command must be executed by an in-game player."));
    } else {
      Player player = (Player)sender;
      
      if(getCommandManager().getPlugin().getGamePlayers().containsKey(player.getUniqueId())) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cYou've already signed up to join the next event!"));
      } else {
        GamePlayer gamePlayer = new GamePlayer(player);
        getCommandManager().getPlugin().getGamePlayers().put(player.getUniqueId(), gamePlayer);

        Location location = player.getLocation();
        MassPlayerTeleport tpRunner = new MassPlayerTeleport();
        tpRunner.addPlayer(gamePlayer);
        boolean doSchedule = false;
        
        if(getCommandManager().getPlugin().getGameManagers().isEmpty()) {
          Entry<String, ProtectedRegion> lobby = getCommandManager().getPlugin().getLobby();
          if(!lobby.getKey().equals(location.getWorld().getName())
              || !lobby.getValue().contains(
                  location.getBlockX(),
                  location.getBlockY(),
                  location.getBlockZ())) {
            tpRunner.addDestination(Bukkit.getWorld(lobby.getKey()), lobby.getValue());
            doSchedule = true;
          }
        } else {
          Set<ProtectedRegion> regions = new HashSet<>();
          GameMap map = getCommandManager().getPlugin().getGameManagers().get(0).getGame().getMap();
          if(!location.getWorld().getName().equals(map.getWorld().getKey())) {
            regions.add(map.getRegion().getValue());
            for(SerializableRegion region : map.getSpectatorRegions())
              regions.add(region.getRegion().getValue());
            boolean found = false;
            for(ProtectedRegion region : regions)
              if(region.contains(
                  location.getBlockX(),
                  location.getBlockY(),
                  location.getBlockZ())) {
                found = true;
                break;
              }
            if(!found) {
              regions.remove(map.getRegion().getValue());
              tpRunner.addDestinations(map.getWorld().getValue(), regions);
              doSchedule = true;
            }
          }
        }
        
        if(doSchedule)
          Bukkit.getScheduler().scheduleSyncDelayedTask(getCommandManager().getPlugin(), tpRunner);
        
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &aYou've signed up to join the next event!"));
      }
      
      return true;
    }
    
    return false;
  }

}
