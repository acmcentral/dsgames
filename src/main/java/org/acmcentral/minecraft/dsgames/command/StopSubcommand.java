package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;

public class StopSubcommand extends Subcommand {
  
  public StopSubcommand(CommandManager commandManager) {
    super(commandManager, "stop an event");
    Map<String, String> usages = new HashMap<>();
    usages.put("", "stop an event");
    setUsages(usages);
  }
  
  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(args.length == 1) {
      if(getCommandManager().getPlugin().getGameManagers().isEmpty())
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &eNo event is currently running."));
      else {
        getCommandManager().getPlugin().getGameManagers().forEach(gm -> gm.stop());
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &aThe event has been stopped."));
      }
      
      return true;
    }
    
    return false;
  }
  
}
