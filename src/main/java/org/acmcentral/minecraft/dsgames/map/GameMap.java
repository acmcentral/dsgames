package org.acmcentral.minecraft.dsgames.map;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class GameMap extends SerializableRegion {

  private Set<ProtectedRegion> spawnRegions = new CopyOnWriteArraySet<>();
  private Set<SerializableRegion> spectatorRegions = new CopyOnWriteArraySet<>();
  
  public GameMap(String world, String name) {
    super(world, name);
  }
  
  public GameMap(JSONObject serialized) {
    super(serialized);
    serialized.getJSONArray("spectatorRegions").forEach(o ->
      spectatorRegions.add(new SerializableRegion((JSONObject)o)));
  }
  
  public JSONObject serialize() {
    JSONArray spectatorRegions = new JSONArray();
    this.spectatorRegions.forEach(r -> spectatorRegions.put(r.serialize()));
    return super.serialize()
        .put("spectatorRegions", spectatorRegions);
  }
  
  public boolean load() {
    boolean success = super.load();
    
    if(success) {
      BooleanFlag spawnFlag = (BooleanFlag)WorldGuard.getInstance().getFlagRegistry().get("ds-arena-spawn");
      intersecting.forEach(r -> {
        Boolean result = r.getFlag(spawnFlag);
        if(result != null && result)
          spawnRegions.add(r);
      });
    }
    
    for(SerializableRegion region : spectatorRegions)
      success &= region.load();
    
    return success;
  }
  
  public Set<ProtectedRegion> getSpawnRegions() {
    return spawnRegions;
  }
  
  public Set<SerializableRegion> getSpectatorRegions() {
    return spectatorRegions;
  }
  
}
