package org.acmcentral.minecraft.dsgames.microplugin.tag.state;

import java.util.Set;

import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public abstract class TagListener extends State implements Listener {
  
  public TagListener(Microplugin microplugin) {
    super(microplugin);
  }

  @EventHandler public void onPlayerTagged(EntityDamageEvent event) {
    if(!(event.getEntity() instanceof Player)) return;
    // TODO only execute this if player is in arena and is playing game
    event.setCancelled(true);
    // TODO here pass this to the queue to be processed
  }

  @Override public Set<Listener> getListeners() {
    if(listeners.size() == 0) listeners.add(this);
    return super.getListeners();
  }
  
}
