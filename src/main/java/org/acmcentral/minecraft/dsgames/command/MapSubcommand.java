package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.Map;

import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class MapSubcommand extends Subcommand {
  
  public MapSubcommand(CommandManager commandManager) {
    super(commandManager, "create, modify, and delete maps");
    Map<String, String> usages = new HashMap<>();
    usages.put("set <microplugin> <region> [world]", "associates a microplugin with a region");
    usages.put("unset <microplugin>", "deassociates any map previously associated with a microplugin");
    setUsages(usages);
  }

  /*
   * TODO there are quite a few additions that can be made to this command.
   * For example:
   * - allowing the administrator to teleport to the region
   * - allowing the administrator to "activate" or "deactivate" a map
   * - wiping an entire map from the microplugin
   */
  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(args.length == 1) {
      StringBuilder buf = new StringBuilder("&2[&4DSGames&2] &aMaps:\n");
      for(String map : getCommandManager().getPlugin().getMapManager().getMapNames()) {
        boolean inUse = !getCommandManager().getPlugin().getMicropluginManager().getMicroplugins().isEmpty();
        buf.append(String.format("&d- &e%1$s&d: %2$s\n", map, inUse ? "&aIn use." : "&cNot in use."));
      }
      sender.sendMessage(ChatColor.translateAlternateColorCodes('&', buf.toString()));
      return true;
    } else if(args.length == 3 && args[1].equalsIgnoreCase("set")) {
      if(!(sender instanceof Player))
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cThe console is required to specify a world."));
      else return onCommand(sender, command, label, new String[] {
         args[0], args[1], args[2], ((Player)sender).getWorld().getName() 
      });
    } else if(args.length == 4 && args[1].equalsIgnoreCase("set")) {
      String microplugin = args[2];
      String region = args[3];
      String world = args[4];
      if(!getCommandManager().getPlugin().getMicropluginManager().getMicroplugins().containsKey(microplugin)) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cThat particular microplugin has not been properly registered."));
      } else if(getCommandManager().getPlugin().getMicropluginManager().getMap(microplugin) != null) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cThe specified microplugin already has a map associated with it."));
      } else {
        GameMap map = getCommandManager().getPlugin().getMapManager().getMap(region);
        boolean success = true;
        if(map == null) {
          map = new GameMap(world, region);
          success = map.load();
          if(success) getCommandManager().getPlugin().getMapManager().addMap(map);
        }
        if(success) {
          getCommandManager().getPlugin().getMicropluginManager().linkMap(microplugin, map);
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
              "&2[&4DSGames&2] &aSuccessfully linked the microplugin to its new map!"));
        } else sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cCould not link the microplugin to thhe specified map."));
      }
      return true;
    } else if(args.length == 4 && args[1].equalsIgnoreCase("unset")) {
      String microplugin = args[2];
      if(getCommandManager().getPlugin().getMicropluginManager().unlinkMicroplugin(microplugin))
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &aSuccessfully unlinked the microplugin from its map."));
      else
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &eThat microplugin already has no map associated with it."));
      return true;
    }
    
    return false;
  }

}
