package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.thread.MassPlayerTeleport;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public class LeaveSubcommand extends Subcommand {

  public LeaveSubcommand(CommandManager commandManager) {
    super(commandManager, "leave a team");
    Map<String, String> usages = new HashMap<>();
    usages.put("", "leave the game");
    setUsages(usages);
  }

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(sender instanceof Player) {
      sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
          "&2[&4DSGames&2] &cThis command must be executed by an in-game player."));
    } else {
      Player player = (Player)sender;
      
      if(getCommandManager().getPlugin().getGamePlayers().containsKey(player.getUniqueId())) {
        GamePlayer gamePlayer = getCommandManager().getPlugin().getGamePlayers().remove(player.getUniqueId());
        Location location = gamePlayer.getPlayer().getLocation();
        
        Entry<String, ProtectedRegion> lobby = getCommandManager().getPlugin().getLobby();
        if(!lobby.getKey().equals(location.getWorld().getName())
            || !lobby.getValue().contains(
                location.getBlockX(),
                location.getBlockY(),
                location.getBlockZ())) {
          MassPlayerTeleport tpRunner = new MassPlayerTeleport();
          tpRunner.addPlayer(gamePlayer);
          tpRunner.addDestination(Bukkit.getWorld(lobby.getKey()), lobby.getValue());
          Bukkit.getScheduler().scheduleSyncDelayedTask(getCommandManager().getPlugin(), tpRunner);
        }
        
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &aYou are now no longer signed up for the next event."));
      } else {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cYou aren't currently signed up to join an event."));
      }
      
      return true;
    }
    
    return false;
  }

}
