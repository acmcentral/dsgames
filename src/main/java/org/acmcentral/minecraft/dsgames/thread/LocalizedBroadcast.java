package org.acmcentral.minecraft.dsgames.thread;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class LocalizedBroadcast implements Runnable {
  
  private Map<Set<UUID>, Set<Entry<BroadcastAction, Object>>> broadcasts = null;
  
  private enum BroadcastAction {
    CHAT_MESSAGE,
    TITLE_MESSAGE,
    ACTIONBAR_MESSAGE,
    NOTE,
    SOUND,
    XP_BAR,
    HEALTH_BOOST
  }
  
  public LocalizedBroadcast() {
    this.broadcasts = new HashMap<>();
  }
  
  @Override public void run() {
    for(Set<UUID> players : broadcasts.keySet())
      for(UUID uuid : players) {
        Player player = Bukkit.getServer().getPlayer(uuid);
        if(player == null || !player.isOnline()) continue;
        
        for(Entry<BroadcastAction, Object> entry : broadcasts.get(players)) {
          switch(entry.getKey()) {
          case ACTIONBAR_MESSAGE:
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
                TextComponent.fromLegacyText((String)entry.getValue()));
            break;
          case CHAT_MESSAGE:
            player.sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&2[&DSGames&2] &r"
                  + (String)entry.getValue()));
            break;
          case NOTE:
            @SuppressWarnings("unchecked")
            Entry<Instrument, Note> data = (Entry<Instrument, Note>)entry.getValue();
            player.playNote(player.getLocation(), data.getKey(), data.getValue());
            break;
          case SOUND:
            Sound sound = (Sound)entry.getValue(); 
            player.playSound(player.getLocation(), sound, 1, 0);
          case TITLE_MESSAGE:
            String[] messages = (String[])entry.getValue();
            player.sendTitle(messages[0], messages[1], 10, 70, 40);
            break;
          case XP_BAR:
            player.setExp((float)entry.getValue());
          case HEALTH_BOOST:
            float percentBoost = (float)entry.getValue();
            int newHealth = (int)(player.getHealth() + percentBoost * player.getMaxHealth() + 1);
            if(newHealth >= player.getMaxHealth()) player.setHealth(player.getMaxHealth());
            else player.setHealth(newHealth);
          }
        }
      }
  }
  
  private Set<Entry<BroadcastAction, Object>> queueAction(Collection<GamePlayer> players, BroadcastAction action, Object data) {
    Set<UUID> uuids = new HashSet<>();
    for(GamePlayer player : players) uuids.add(player.getUUID());
    
    Set<Entry<BroadcastAction, Object>> actions = null;
    if(broadcasts.containsKey(players))
      actions = broadcasts.get(players);
    else {
      actions = new HashSet<>();
      broadcasts.put(uuids, actions);
    }
    
    actions.add(new SimpleEntry<>(action, data));
    
    return actions;
  }
  
  public LocalizedBroadcast addChatMessageBroadcast(Collection<GamePlayer> players, String message) {
    queueAction(players,
        BroadcastAction.CHAT_MESSAGE,
        message);
    return this;
  }
  
  public LocalizedBroadcast addTitleMessageBroadcast(Collection<GamePlayer> players, String title, String subtitle) {
    queueAction(players,
        BroadcastAction.TITLE_MESSAGE,
        new String[] { title, subtitle });
    return this;
  }
  
  public LocalizedBroadcast addActionbarBroadcast(Collection<GamePlayer> players, String message) {
    queueAction(players,
        BroadcastAction.ACTIONBAR_MESSAGE,
        message);
    return this;
  }
  
  public LocalizedBroadcast addNoteBroadcast(Collection<GamePlayer> players, Instrument instrument, Note note) {
    queueAction(players,
        BroadcastAction.NOTE,
        new SimpleEntry<Instrument, Note>(instrument, note));
    return this;
  }
  
  public LocalizedBroadcast addSoundBroadcast(Collection<GamePlayer> players, Sound sound) {
    queueAction(players,
        BroadcastAction.SOUND,
        sound);
    return this;
  }
  
  public LocalizedBroadcast addXPBroadcast(Collection<GamePlayer> players, float percentage) {
    queueAction(players,
        BroadcastAction.XP_BAR,
        percentage);
    return this;
  }
  
  public LocalizedBroadcast addHealthBroadcast(Collection<GamePlayer> players, float percentage) {
    queueAction(players,
        BroadcastAction.HEALTH_BOOST,
        percentage);
    return this;
  }
  
}
