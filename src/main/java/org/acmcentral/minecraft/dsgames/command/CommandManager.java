package org.acmcentral.minecraft.dsgames.command;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acmcentral.minecraft.dsgames.DSGames;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;

public class CommandManager implements CommandExecutor {
  
  private DSGames plugin = null;
  private Map<String, Subcommand> subcommands = null;
  
  public CommandManager(DSGames plugin) {
    this.plugin = plugin;
    this.subcommands = new LinkedHashMap<>();
    subcommands.put("join", new JoinSubcommand(this));
    subcommands.put("leave", new LeaveSubcommand(this));
    subcommands.put("map", new MapSubcommand(this));
    subcommands.put("help", new HelpSubcommand(this));
  }
  
  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    final List<String> labels = Arrays.asList("dsgames", "event", "events", "game", "games");
    
    if(labels.contains(label.toLowerCase())
        && args.length > 1
        && subcommands.containsKey(args[1].toLowerCase()))
      return subcommands.get(args[1].toLowerCase()).onCommand(sender, command, label, args);
    sender.sendMessage(
        ChatColor.translateAlternateColorCodes('&',
            String.format("&2[&cDSGames&2] &4Syntax error. &7Please use &e/%1$s help &7to learn more.",
                label)));
    return true;
  }
  
  public Map<String, Subcommand> getSubcommands() {
    return subcommands;
  }
  
  public DSGames getPlugin() {
    return plugin;
  }

}
