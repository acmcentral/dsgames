package org.acmcentral.minecraft.dsgames.microplugin.tag;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.acmcentral.minecraft.dsgames.microplugin.tag.state.EndGame;
import org.acmcentral.minecraft.dsgames.microplugin.tag.state.MainStage;

public class TagMicroplugin extends Microplugin {
  
  private MainStage mainStage = null;
  
  public TagMicroplugin(GameManager gameManager) {
    super(gameManager);
    EndGame endGame = new EndGame(this);
    mainStage = new MainStage(this);
    mainStage.setNextState(endGame);
  }
  
  @Override public String getName() {
    return "Tag";
  }
  
  @Override public State getFirstState() {
    return mainStage;
  }

  @Override public State getCleanupState() {
    return null;
  }

}
