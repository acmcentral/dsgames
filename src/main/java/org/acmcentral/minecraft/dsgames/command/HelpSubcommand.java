package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.Map;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;

public class HelpSubcommand extends Subcommand {

  public HelpSubcommand(CommandManager commandManager) {
    super(commandManager, "learn about the commands");
    Map<String, String> usages = new HashMap<>();
    usages.put("", "displays an overview of all commands");
    usages.put("<command>", "displays more info about a particular command");
    setUsages(usages);
  }

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Map<String, Subcommand> subcommands = getCommandManager().getSubcommands();
    if(args.length == 1) {
      sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2[&4DSGames&2] &eHelp:"));
      for(String c : subcommands.keySet()) {
        Subcommand subcommand = subcommands.get(c);
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &a" + c + ": &e" + subcommand.getDescription()));
      }
      return true;
    } else if(args.length == 2 && subcommands.containsKey(args[1].toLowerCase())) {
      Subcommand subcommand = subcommands.get(args[1].toLowerCase());
      Map<String, String> usages = subcommand.getUsages();
      sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2[&4DSGames&2] &eHelp for &d" + label + " " + args[1].toLowerCase()));
      for(String usage : usages.keySet())
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2[&4DSGames&2] &a" + label + " " + usage + ": &e" + usages.get(usage)));
      return true;
    }
    
    return false;
  }

}
