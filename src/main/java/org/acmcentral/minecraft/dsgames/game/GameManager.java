package org.acmcentral.minecraft.dsgames.game;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.acmcentral.minecraft.dsgames.DSGames;
import org.acmcentral.minecraft.dsgames.game.state.LobbyState;
import org.acmcentral.minecraft.dsgames.game.state.PostgameState;
import org.acmcentral.minecraft.dsgames.game.state.PregameState;
import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class GameManager implements Runnable {
  
  private DSGames plugin = null;
  private GameMap map = null;
  private Map<GamePlayer, Integer> scores = new ConcurrentHashMap<>();
  private Microplugin game = null;
  private LobbyState lobbyState = null;
  private PregameState pregameState = null;
  private PostgameState postgameState = null;
  private Thread thread = null;
  
  public GameManager(DSGames plugin, Class<?> microplugin) throws Exception {
    this.plugin = plugin;
    
    this.game = (Microplugin)microplugin.getConstructor(GameManager.class).newInstance(this);
    GameMap map = plugin.getMicropluginManager().getMap(microplugin.getName());
    if(map == null) throw new Exception("This microplugin is not linked to a map.");
    this.map = map;
    
    this.lobbyState = new LobbyState(this.game);
    this.pregameState = new PregameState(this.game);
    this.postgameState = new PostgameState(this.game);
    
    this.lobbyState.setNextState(pregameState);
    this.pregameState.setNextState(game.getFirstState());
    this.postgameState.setNextState(game.getCleanupState());
  }
  
  public boolean start() {
    if(thread != null) return false;
    thread = new Thread(this);
    thread.setDaemon(true);
    thread.start();
    return true;
  }
  
  private void execute(State state, Map<String, Object> register) {
    while(state != null) {
      try {
        Set<Listener> listeners = state.getListeners();
        listeners.forEach(l -> plugin.getServer().getPluginManager().registerEvents(l, plugin));
        if(state.prepare(scores, register)) {
          long startTime = System.currentTimeMillis();
          while(thread.isInterrupted() && state.execute(scores, register, System.currentTimeMillis() - startTime))
            Thread.sleep(500L);
          state = state.getNextState();
        } else state = null;
        listeners.forEach(l -> HandlerList.unregisterAll(l));
      } catch(InterruptedException e) { }
    }
  }
  
  @Override public void run() {
    Map<String, Object> register = new ConcurrentHashMap<>();
    execute(lobbyState, register);
    execute(postgameState, register);
  }
  
  public boolean stop() {
    if(thread == null) return false;
    thread.interrupt();
    thread = null;
    return true;
  }
  
  public Microplugin getGame() {
    return game;
  }
  
  public DSGames getPlugin() {
    return plugin;
  }

}
