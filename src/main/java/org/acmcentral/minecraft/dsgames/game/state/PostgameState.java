package org.acmcentral.minecraft.dsgames.game.state;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;

public class PostgameState extends State {
  
  public PostgameState(Microplugin microplugin) {
    super(microplugin);
  }

  @Override public boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register) {
    return true;
  }
  
  @Override public boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds) {
    // TODO teleport people to lobby
    return true;
  }
  
}
