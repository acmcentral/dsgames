package org.acmcentral.minecraft.dsgames.game.state;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.acmcentral.minecraft.dsgames.thread.LocalizedBroadcast;
import org.bukkit.Bukkit;

public class LobbyState extends State {
  
  private boolean lastWarning = false;
  
  public LobbyState(Microplugin microplugin) {
    super(microplugin);
  }

  @Override public boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register) {
    LocalizedBroadcast broadcast = new LocalizedBroadcast()
        .addChatMessageBroadcast(getMicroplugin().getGameManager().getPlugin().getGamePlayers().values(), "The event is going to start in twenty seconds.");
    Bukkit.getScheduler().scheduleSyncDelayedTask(getMicroplugin().getGameManager().getPlugin(), broadcast);
    return true;
  }
  
  @Override public boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds) {
    if(milliseconds > 20 * 1000L) return false;
    if(milliseconds > 15 * 1000L && !lastWarning) {
      LocalizedBroadcast broadcast = new LocalizedBroadcast()
          .addChatMessageBroadcast(getMicroplugin().getGameManager().getPlugin().getGamePlayers().values(), "The event is going to start in five seconds.");
      Bukkit.getScheduler().scheduleSyncDelayedTask(getMicroplugin().getGameManager().getPlugin(), broadcast);
    }
    return true;
  }
  
}
