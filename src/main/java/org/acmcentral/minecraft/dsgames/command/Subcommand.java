package org.acmcentral.minecraft.dsgames.command;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.bukkit.command.CommandExecutor;

public abstract class Subcommand implements CommandExecutor {
  
  private CommandManager commandManager = null;
  private String description = null;
  private Map<String, String> usages = null;
  
  public Subcommand(CommandManager commandManager, String description) {
    this.description = description;
  }
  
  protected CommandManager getCommandManager() {
    return commandManager;
  }
  
  public Map<String, String> getUsages() {
    return usages;
  }
  
  public void setUsages(Map<String, String> usages) {
    this.usages = usages;
  }
  
  public String getDescription() {
    return description;
  }
  
}
