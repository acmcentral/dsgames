package org.acmcentral.minecraft.dsgames;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.map.MapManager;
import org.acmcentral.minecraft.dsgames.microplugin.MicropluginManager;
import org.acmcentral.minecraft.dsgames.persistent.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONException;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class DSGames extends JavaPlugin {
  
  public static final Logger log = Bukkit.getLogger();
  
  private Entry<String, ProtectedRegion> lobby = null;
  private Config config = new Config(getDataFolder(), "config.json");
  private List<GameManager> gameManagers = new CopyOnWriteArrayList<>();
  private MapManager mapManager = new MapManager();
  private MicropluginManager micropluginManager = null;
  private Map<UUID, GamePlayer> gamePlayers = new ConcurrentHashMap<>();
  
  @Override public void onLoad() {
    if(!getServer().getPluginManager().isPluginEnabled("WorldGuard"))
      try {
        FlagRegistry flags = WorldGuard.getInstance().getFlagRegistry();
        BooleanFlag arenaSpawnFlag = new BooleanFlag("ds-arena-spawn");
        flags.register(arenaSpawnFlag);
      } catch(FlagConflictException e) {
        e.printStackTrace();
        getServer().getPluginManager().disablePlugin(this);
      }
  }
  
  @Override public void onEnable() {
    log.info("It's alive!!!!");
    
    try {
      if(!config.load()) config.save();
      if(config.getData().has("maps"))
        mapManager.load(config.getData().getJSONArray("maps"));
      micropluginManager = config.getData().has("plugins")
          ? new MicropluginManager(this)
          : new MicropluginManager(this, config.getData().getJSONArray("plugins"));
    } catch(JSONException e) {
      e.printStackTrace();
      getServer().getPluginManager().disablePlugin(this);
    }
    
    // TODO eventually, read these from JAR files in the configuration folder
//    Microplugin tagMicroplugin = new TagMicroplugin();
//    microplugins.add(tagMicroplugin);
//    GameManager manager = GameManager.build(tagMicroplugin.getClass());
    
  }
  
  @Override public void onDisable() {
    log.info("Bye, I won't be back.");
  }
  
  @Override public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
//    if(cmd.getName().equalsIgnoreCase("testme"))
//      sender.sendMessage("bugger off");
//    else
//      sender.sendMessage("no really, bugger off.");
    
    return true;
  }
  
  public Entry<String, ProtectedRegion> getLobby() {
    return lobby;
  }
  
  public MapManager getMapManager() {
    return mapManager;
  }
  
  public MicropluginManager getMicropluginManager() {
    return micropluginManager;
  }
  
  public List<GameManager> getGameManagers() {
    return gameManagers;
  }
  
  public Map<UUID, GamePlayer> getGamePlayers() {
    return gamePlayers;
  }
  
}
