package org.acmcentral.minecraft.dsgames.map;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class GamePlayer implements Comparable<GamePlayer> {
  
  private AtomicReference<Player> player = new AtomicReference<>();
  private AtomicReference<String> ign = new AtomicReference<>();
  private AtomicReference<UUID> uuid = new AtomicReference<>();
  
  public GamePlayer() {
    this.player = new AtomicReference<>();
    this.ign = new AtomicReference<>();
    this.uuid = new AtomicReference<>();
  }
  
  public GamePlayer(Player player) {
    this.player = new AtomicReference<>(player);
    this.ign = new AtomicReference<>(player.getName());
    this.uuid = new AtomicReference<>(player.getUniqueId());
  }
  
  public Player getPlayer() {
    return player.get();
  }
  
  public GamePlayer setPlayer(Player player) {
    this.player.set(player);
    return this;
  }
  
  public String getIGN() {
    return ign.get();
  }
  
  public GamePlayer setIGN(String ign) {
    this.ign.set(ign);
    return this;
  }
  
  public UUID getUUID() {
    return uuid.get();
  }
  
  public GamePlayer setUUID(UUID uuid) {
    this.uuid.set(uuid);
    return this;
  }
  
  public void sendMessage(String message) {
    Player player = this.player.get();
    if(player != null && player.isOnline())
      player.sendMessage(
          ChatColor.translateAlternateColorCodes('&', "&2[&4DSGames&2] &r" + message));
  }
  
  public void sendTitle(String title, String subtitle) {
    Player player = this.player.get();
    if(player != null && player.isOnline())
      player.sendTitle(title, subtitle, 10, 70, 20);
  }
  
  public void sendActionBar(String message) {
    Player player = this.player.get();
    if(player != null && player.isOnline())
      player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
  }
  
  public void teleport(Location location) {
    Player player = this.player.get();
    if(player != null && player.isOnline())
      player.teleport(location);
  }
  
  @Override public boolean equals(Object o) {
    if(uuid.get() == null) throw new NullPointerException();
    if(!(o instanceof GamePlayer)) return false;
    GamePlayer player = (GamePlayer)o;
    if(player.uuid.get() == null) throw new NullPointerException();
    return uuid.get().compareTo(player.uuid.get()) == 0;
  }
  
  @Override public int hashCode() {
    if(uuid.get() == null) throw new NullPointerException();
    return uuid.get().hashCode();
  }

  @Override public int compareTo(GamePlayer o) {
    if(o == null || o.uuid.get() == null || uuid.get() == null) throw new NullPointerException();
    return uuid.get().compareTo(o.uuid.get());
  }
  
}
