package org.acmcentral.minecraft.dsgames.persistent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONException;
import org.json.JSONObject;

public class Config {
  
  private File folder = null;
  private String filename = null;
  private AtomicReference<JSONObject> data = new AtomicReference<>(new JSONObject());
  
  public Config(File folder, String filename) {
    this.folder = folder;
    this.filename = filename;
  }
  
  public boolean load() throws JSONException {
    if(folder.exists()) {
      File file = new File(folder, filename);
      if(file.isFile() && file.canRead()) {
        try(Scanner scanner = new Scanner(file)) {
          StringBuilder raw = new StringBuilder();
          while(scanner.hasNext()) raw.append(scanner.nextLine());
          JSONObject serialized = new JSONObject(scanner.toString());
          data.set(serialized);
          return true;
        } catch(FileNotFoundException e) { }
      }
    }
    
    return false;
  }
  
  public boolean save() {
    JSONObject serialized = data.get();
    if(folder.exists() || folder.mkdirs()) {
      File file = new File(folder, filename);
      try {
        if(file.exists() || file.createNewFile())
          try(PrintWriter printWriter = new PrintWriter(file)) {
            printWriter.println(data.get().toString(2));
            return true;
          }
      } catch(IOException e) { }
    }
    
    return false;
  }
  
  public JSONObject getData() {
    return data.get();
  }
  
}
