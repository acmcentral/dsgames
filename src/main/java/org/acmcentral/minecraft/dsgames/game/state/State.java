package org.acmcentral.minecraft.dsgames.game.state;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.bukkit.event.Listener;

public abstract class State {

  private State nextState = null;
  private Microplugin microplugin = null;
  protected Set<Listener> listeners = new HashSet<>();
  
  public State(Microplugin microplugin) {
    this.microplugin = microplugin;
  }
  
  public State getNextState() {
    return nextState;
  }
  
  public void setNextState(State state) {
    this.nextState = state;
  }
  
  public Set<Listener> getListeners() {
    return listeners;
  }
  
  public abstract boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register);
  
  public abstract boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds);
  
  protected Microplugin getMicroplugin() {
    return microplugin;
  }
  
}
