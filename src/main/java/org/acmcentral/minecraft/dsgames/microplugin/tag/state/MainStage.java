package org.acmcentral.minecraft.dsgames.microplugin.tag.state;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;

public class MainStage extends TagListener {
  
  public MainStage(Microplugin microplugin) {
    super(microplugin);
  }

  @Override public boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register) {
    return true;
  }
  
  @Override public boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds) {
    return true;
  }
  
}
