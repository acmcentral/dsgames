package org.acmcentral.minecraft.dsgames.thread;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class MassPlayerTeleport implements Runnable {
  
  private List<GamePlayer> players = new CopyOnWriteArrayList<>();
  private List<Entry<ProtectedRegion, World>> destinations = new CopyOnWriteArrayList<>();
  private Map<ProtectedRegion, List<Location>> safeSpaces = new ConcurrentHashMap<>();
  
  @Override public void run() {
    Collections.shuffle(destinations);
    for(Entry<ProtectedRegion, World> destination : destinations) {
      ProtectedRegion region = destination.getKey();
      if(safeSpaces.containsKey(region)) continue;
      World world = destination.getValue();
      List<Location> safeSpaces = new CopyOnWriteArrayList<>();
      int maxY = region.getMaximumPoint().getBlockY();
      int minY = region.getMinimumPoint().getBlockY();
      for(BlockVector2 point : region.getPoints()) {
        for(int y = minY; y <= maxY; y++) {
          Location location = new Location(world, point.getX() + 0.5, y, point.getZ() + 0.5);
          Block floor = world.getBlockAt(location);
          if(floor.isEmpty()
              && floor.getRelative(0, 1, 0).isEmpty()
              && !floor.getRelative(0, -1, 0).isPassable())
            safeSpaces.add(location);
        }
      }
      Collections.shuffle(safeSpaces);
      this.safeSpaces.put(region, safeSpaces);
    }
    
    for(int i = players.size() - 1; i >= 0; i--) {
      List<Location> safeSpaces = this.safeSpaces.get(destinations.get(i % destinations.size()).getKey());
      Location destination = safeSpaces.get(i % safeSpaces.size());
      players.get(i).teleport(destination);
    }
  }
  
  public void addPlayer(GamePlayer player) {
    players.add(player);
  }
  
  public void addPlayers(Collection<GamePlayer> players) {
    players.forEach(p -> addPlayer(p));
  }
  
  public void removePlayer(GamePlayer player) {
    players.remove(player);
  }
  
  public void addDestination(World world, ProtectedRegion region) {
    destinations.add(new SimpleEntry<>(region, world));
  }
  
  public void addDestinations(World world, Collection<ProtectedRegion> regions) {
    regions.forEach(r -> addDestination(world, r));
  }
  
  public void removeDestination(World world, ProtectedRegion region) {
    destinations.remove(new SimpleEntry<>(region, world));
  }
  
}
