package org.acmcentral.minecraft.dsgames.microplugin.anvildrop;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;

public class AnvilDropMicroplugin extends Microplugin {
  
  public AnvilDropMicroplugin(GameManager gameManager) {
    super(gameManager);
  }
  
  @Override public String getName() {
    return "Anvil Drop";
  }
  
  @Override public State getFirstState() {
    return null;
  }
  
  @Override public State getCleanupState() {
    return null;
  }
  
}
