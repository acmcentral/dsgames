package org.acmcentral.minecraft.dsgames.microplugin;

import java.util.concurrent.atomic.AtomicReference;

import org.acmcentral.minecraft.dsgames.DSGames;
import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.acmcentral.minecraft.dsgames.persistent.Config;

public abstract class Microplugin {
  
  private AtomicReference<GameMap> map = new AtomicReference<>();
  private Config config = null;
  private GameManager gameManager = null;
  
  protected Microplugin(GameManager gameManager) {
    this.gameManager = gameManager;
  }
  
  public Config getConfig() {
    return config;
  }
  
  public void setConfig(Config config) {
    this.config = config;
  }
  
  public GameMap getMap() {
    return map.get();
  }
  
  public void setMap(GameMap map) {
    this.map.set(map);
  }
  
  public abstract String getName();
  
  public abstract State getFirstState();
  
  public abstract State getCleanupState();
  
  public GameManager getGameManager() {
    return gameManager;
  }
  
}
