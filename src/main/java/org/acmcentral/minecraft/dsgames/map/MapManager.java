package org.acmcentral.minecraft.dsgames.map;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class MapManager {
  
  private Map<String, GameMap> gameMaps = new ConcurrentHashMap<>();
  
  public boolean load(JSONArray maps) {
    boolean success = true;
    for(Object m : maps) {
      try {
        GameMap map = new GameMap((JSONObject)m);
        gameMaps.put(map.getRegion().getKey(), map);
      } catch(Exception e) {
        e.printStackTrace();
        success = false;
      }
    }
    return success;
  }
  
  public JSONArray serialize() {
    JSONArray maps = new JSONArray();
    gameMaps.forEach((k, v) -> maps.put(v.serialize()));
    return maps;
  }
  
  public GameMap getMap(String map) {
    return gameMaps.get(map);
  }
  
  public Set<String> getMapNames() {
    return gameMaps.keySet();
  }
  
  public boolean addMap(GameMap map) {
    String name = map.getRegion().getKey();
    if(gameMaps.containsKey(name)) return false;
    gameMaps.put(name, map);
    return true;
  }
  
  public boolean removeMap(String map) {
    if(!gameMaps.containsKey(map)) return false;
    gameMaps.remove(map);
    return true;
  }
  
}
