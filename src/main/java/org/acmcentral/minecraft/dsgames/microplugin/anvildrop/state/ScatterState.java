package org.acmcentral.minecraft.dsgames.microplugin.anvildrop.state;

import java.util.Map;

import org.acmcentral.minecraft.dsgames.game.state.State;
import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.acmcentral.minecraft.dsgames.map.GamePlayer;
import org.acmcentral.minecraft.dsgames.microplugin.Microplugin;
import org.acmcentral.minecraft.dsgames.thread.LocalizedBroadcast;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Polygonal2DRegion;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockTypes;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class ScatterState extends State {
  
  private long lastNotification = Long.MAX_VALUE;
  
  public ScatterState(Microplugin microplugin) {
    super(microplugin);
  }

  @Override public boolean prepare(Map<GamePlayer, Integer> scores, Map<String, Object> register) {
    if(!register.containsKey("rounds"))
      register.put("rounds", 1);
    else register.replace("rounds", (Integer)(register.get("rounds")) + 1);
    
    Runnable runnable = new Runnable() {
      @Override public void run() {
        GameMap map = getMicroplugin().getMap();
        ProtectedRegion region = map.getRegion().getValue();
        World world = BukkitAdapter.adapt(map.getWorld().getValue());
        Polygonal2DRegion weRegion = new Polygonal2DRegion(
            world,
            region.getPoints(),
            region.getMinimumPoint().getBlockY() + 1,
            region.getMinimumPoint().getBlockY() + 2);
        try(EditSession editSession = WorldEdit.getInstance().newEditSession(world)) {
          editSession.setBlocks(weRegion, BlockTypes.GREEN_CONCRETE.getDefaultState());
        } catch(MaxChangedBlocksException e) {
          e.printStackTrace();
        }
      }
    };
    
    Bukkit.getScheduler().scheduleSyncDelayedTask(getMicroplugin().getGameManager().getPlugin(), runnable);
    
    try {
      Thread.sleep(1000L);
    } catch(InterruptedException e) { }
    
    return true;
  }

  @Override public boolean execute(Map<GamePlayer, Integer> scores, Map<String, Object> register, long milliseconds) {
    if(milliseconds > 10 * 1000L) {
      setNextState(new DropAnvilState(getMicroplugin()));
      return false;
    }
    
    if(lastNotification > milliseconds - 1000L) {
      lastNotification = milliseconds;
      LocalizedBroadcast broadcast = new LocalizedBroadcast()
          .addActionbarBroadcast(getMicroplugin().getGameManager().getPlugin().getGamePlayers().values(),
              String.format("Scatter State: %1$d milliseconds", milliseconds))
          .addSoundBroadcast(getMicroplugin().getGameManager().getPlugin().getGamePlayers().values(),
              Sound.BLOCK_ANVIL_LAND);
      Bukkit.getScheduler().scheduleSyncDelayedTask(getMicroplugin().getGameManager().getPlugin(), broadcast);
    }
    
    return true;
  }

}
