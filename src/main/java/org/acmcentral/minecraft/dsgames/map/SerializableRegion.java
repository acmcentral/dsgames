package org.acmcentral.minecraft.dsgames.map;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.bukkit.Bukkit;
import org.json.JSONObject;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;

public class SerializableRegion {
  
  private Entry<String, ProtectedRegion> region = null;
  private Entry<String, org.bukkit.World> world = null;
  
  protected Set<ProtectedRegion> intersecting = new CopyOnWriteArraySet<>();
  
  public SerializableRegion(String world, String name) {
    this.region = new SimpleEntry<>(world, null);
    this.world = new SimpleEntry<>(world, null);
  }
  
  public SerializableRegion(JSONObject serialized) {
    this(serialized.getString("world"), serialized.getString("region"));
  }
  
  public JSONObject serialize() {
    return new JSONObject()
        .put("world", world.getKey())
        .put("region", region.getKey());
  }
  
  public boolean load() {
    World world = null;
    org.bukkit.World w = Bukkit.getWorld(this.world.getKey());
    if(w != null) {
      world = BukkitAdapter.adapt(w);
      this.world.setValue(w);
    }
    if(world == null) return false;
    
    RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
    RegionManager regions = container.get(world);
    if(regions == null) return false;
    
    ProtectedRegion region = regions.getRegion(this.region.getKey());
    if(region == null) return false;
    
    intersecting.addAll(region.getIntersectingRegions(regions.getRegions().values()));
    
    this.region.setValue(region);
    return true;
  }
  
  public Entry<String, ProtectedRegion> getRegion() {
    return region;
  }
  
  public Entry<String, org.bukkit.World> getWorld() {
    return world;
  }
  
}
