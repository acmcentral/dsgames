# Damn Small Games

Right now this file's going to serve as my notes. We'll prettify it later.
Or not. depends on my mood.

## Commands

The main command is /dsgames, but it has some aliases: /event, /events, /game, and /games.
These all point to the same command. Subcommands are as follows:
- map -> for all map creation and modification commands
  - set <plugin> <region> -> maps a microplugin 
  - unset <plugin> <region>
- join -> to join the lobby (you can be in the lobby physically, but joining allows for warping and maybe voting in the future, etc)
  - can't be done if the user already joined
- leave -> opposite of "join" subcommand, pretty self explanatory; can't be done if the user hadn't joined in the first place

## Regions

The naming of arena regions is left entirely up to the administrator. What's important is that each arena region either has
the `ds-arena-spawn` flag itself or it's intersected by another region containing it. There will also need to be a region
for spectators to spawn in for each arena.
