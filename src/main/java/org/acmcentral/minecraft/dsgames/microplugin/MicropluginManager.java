package org.acmcentral.minecraft.dsgames.microplugin;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.acmcentral.minecraft.dsgames.DSGames;
import org.acmcentral.minecraft.dsgames.map.GameMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class MicropluginManager {
  
  private Map<String, Class<?>> microplugins = new ConcurrentHashMap<>();
  private Map<String, GameMap> maps = new ConcurrentHashMap<>();
  private Map<GameMap, Set<String>> mappedMicroplugins = new ConcurrentHashMap<>();
  private DSGames plugin = null;
  
  public MicropluginManager(DSGames plugin) {
    microplugins.put("org.acmcentral.minecraft.dsgames.microplugin.tag.TagMicroplugin",
        org.acmcentral.minecraft.dsgames.microplugin.tag.TagMicroplugin.class);
  }
  
  public MicropluginManager(DSGames plugin, JSONArray serialized) {
    this(plugin);
    serialized.forEach(o -> {
      JSONObject jso = (JSONObject)o;
      linkMap(jso.getString("class"), plugin.getMapManager().getMap(jso.getString("map")));
    });
  }
  
  public JSONArray serialize() {
    JSONArray serialized = new JSONArray();
    microplugins.forEach((k, v) -> {
      serialized.put(new JSONObject()
          .put("class", k)
          .put("map", maps.containsKey(k) ? maps.get(k).getRegion().getKey() : JSONObject.NULL));
    });
    return serialized;
  }
  
  public boolean linkMap(String microplugin, GameMap map) {
    if(microplugins.containsKey(microplugin)
        && !maps.containsKey(microplugin)) {
      maps.put(microplugin, map);
      if(!mappedMicroplugins.containsKey(map))
        mappedMicroplugins.put(map, new CopyOnWriteArraySet<>());
      mappedMicroplugins.get(map).add(microplugin);
      return true;
    }
    
    return false;
  }
  
  public boolean unlinkMicroplugin(String microplugin) {
    if(!maps.containsKey(microplugin)) return false;
    GameMap map = maps.remove(microplugin);
    if(map != null) {
      Set<String> mappedMicroplugins = this.mappedMicroplugins.get(map);
      if(mappedMicroplugins.size() == 1)
        this.mappedMicroplugins.remove(map);
      else mappedMicroplugins.remove(microplugin);
    }
    return true;
  }
  
  public boolean unlinkMap(GameMap map) {
    if(!mappedMicroplugins.containsKey(map)) return false;
    Set<String> mappedMicroplugins = this.mappedMicroplugins.remove(map);
    mappedMicroplugins.forEach(m -> maps.remove(m));
    return true;
  }
  
  public GameMap getMap(String microplugin) {
    if(!maps.containsKey(microplugin)) return null;
    return maps.get(microplugin);
  }
  
  public Map<String, Class<?>> getMicroplugins() {
    return microplugins;
  }
  
}
