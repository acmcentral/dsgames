package org.acmcentral.minecraft.dsgames.command;

import java.util.HashMap;
import java.util.Map;

import org.acmcentral.minecraft.dsgames.game.GameManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;

public class StartSubcommand extends Subcommand {
  
  public StartSubcommand(CommandManager commandManager) {
    super(commandManager, "start an event");
    Map<String, String> usages = new HashMap<>();
    usages.put("<event>", "starts a particular event");
    setUsages(usages);
  }
  
  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Class<?> microplugin = getCommandManager().getPlugin().getMicropluginManager().getMicroplugins().get(args[2]);
    if(args.length == 2) {
      if(microplugin == null)
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cThat microplugin has not been installed."));
      else if(!getCommandManager().getPlugin().getGameManagers().isEmpty())
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
            "&2[&4DSGames&2] &cAn event is already running."));
      else {
        GameManager game = null;
        
        try {
          game = new GameManager(getCommandManager().getPlugin(), microplugin);
          getCommandManager().getPlugin().getGameManagers().add(game);
          game.start();
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
              "&2[&4DSGames&2] &aThe event has started!"));  
        } catch(Exception e) {
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
              String.format("&2[&4DSGames&2] &cThe event could not be started: %1$s",
                  e.getMessage())));
          e.printStackTrace();
          try {
            game.stop();
          } catch(Exception e2) { }
        }
      }
      
      return true;
    }
    
    return false;
  }
  
}
